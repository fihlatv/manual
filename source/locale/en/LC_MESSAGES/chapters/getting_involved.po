# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2011-2015, The Mixxx Development Team
# This file is distributed under the same license as the Mixxx package.
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Mixxx DJ manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-11-24 17:11+0100\n"
"PO-Revision-Date: 2015-11-24 12:15+0000\n"
"Last-Translator: S.Brandt <s.brandt@mixxx.org>\n"
"Language-Team: English (http://www.transifex.com/mixxx-dj-software/mixxxdj-manual/language/en/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../source/chapters/getting_involved.rst:4
msgid "Contributing to the Project"
msgstr "Contributing to the Project"

#: ../../source/chapters/getting_involved.rst:6
msgid ""
"Mixxx is a community-driven project involving many DJs worldwide. Without "
"the contributions from these DJs, Mixxx would not exist, and we're "
"constantly looking for more contributors."
msgstr "Mixxx is a community-driven project involving many DJs worldwide. Without the contributions from these DJs, Mixxx would not exist, and we're constantly looking for more contributors."

#: ../../source/chapters/getting_involved.rst:10
msgid ""
"We want to make it as easy as possible for people to get involved in Mixxx "
"development.  First off, to be clear **you don't have to be a programmer to "
"contribute**."
msgstr "We want to make it as easy as possible for people to get involved in Mixxx development.  First off, to be clear **you don't have to be a programmer to contribute**."

#: ../../source/chapters/getting_involved.rst:15
msgid "As a non-developer"
msgstr "As a non-developer"

#: ../../source/chapters/getting_involved.rst:17
msgid ""
"`Donate <http://mixxx.org/download/>`_ to Mixxx using Paypal to help support"
" and enhance development."
msgstr "`Donate <http://mixxx.org/download/>`_ to Mixxx using Paypal to help support and enhance development."

#: ../../source/chapters/getting_involved.rst:19
msgid "`Make skins <http://mixxx.org/wiki/doku.php/creating_skins>`_"
msgstr "`Make skins <http://mixxx.org/wiki/doku.php/creating_skins>`_"

#: ../../source/chapters/getting_involved.rst:20
msgid ""
"`Make MIDI mappings "
"<http://mixxx.org/wiki/doku.php/midi_controller_mapping_file_format>`_ for "
"your controller"
msgstr "`Make MIDI mappings <http://mixxx.org/wiki/doku.php/midi_controller_mapping_file_format>`_ for your controller"

#: ../../source/chapters/getting_involved.rst:23
msgid "`Report bugs <https://bugs.launchpad.net/mixxx>`_"
msgstr "`Report bugs <https://bugs.launchpad.net/mixxx>`_"

#: ../../source/chapters/getting_involved.rst:24
msgid ""
"Update our `Wiki <http://mixxx.org/wiki/doku.php>`_ to make sure the "
"information on it is up to date."
msgstr "Update our `Wiki <http://mixxx.org/wiki/doku.php>`_ to make sure the information on it is up to date."

#: ../../source/chapters/getting_involved.rst:26
msgid ""
"Translate Mixxx using `Transifex <https://www.transifex.com/mixxx-dj-"
"software/public/>`_"
msgstr "Translate Mixxx using `Transifex <https://www.transifex.com/mixxx-dj-software/public/>`_"

#: ../../source/chapters/getting_involved.rst:28
msgid ""
"Answer questions on the `Troubleshooting Forum "
"<http://mixxx.org/forums/viewforum.php?f=3>`_ and the `Launchpad Answers "
"page <https://answers.launchpad.net/mixxx>`_"
msgstr "Answer questions on the `Troubleshooting Forum <http://mixxx.org/forums/viewforum.php?f=3>`_ and the `Launchpad Answers page <https://answers.launchpad.net/mixxx>`_"

#: ../../source/chapters/getting_involved.rst:31
msgid ""
"Help promote Mixxx: If you've got a blog, write an article about Mixxx. Blog"
" about our new releases when they come out. Any exposure on the web and in "
"print helps our project grow, and is much appreciated."
msgstr "Help promote Mixxx: If you've got a blog, write an article about Mixxx. Blog about our new releases when they come out. Any exposure on the web and in print helps our project grow, and is much appreciated."

#: ../../source/chapters/getting_involved.rst:34
msgid "Send us some photos of you using Mixxx at a gig!"
msgstr "Send us some photos of you using Mixxx at a gig!"

#: ../../source/chapters/getting_involved.rst:37
msgid "As a software developer"
msgstr "As a software developer"

#: ../../source/chapters/getting_involved.rst:39
msgid ""
"You don't have to know C++. There are developers who got into Mixxx "
"development while learning C++ along the way."
msgstr "You don't have to know C++. There are developers who got into Mixxx development while learning C++ along the way."

#: ../../source/chapters/getting_involved.rst:41
msgid ""
"Join our mailing list: `mixxx-devel "
"<https://lists.sourceforge.net/lists/listinfo/mixxx-devel>`_"
msgstr "Join our mailing list: `mixxx-devel <https://lists.sourceforge.net/lists/listinfo/mixxx-devel>`_"

#: ../../source/chapters/getting_involved.rst:43
msgid ""
"Join our :term:`IRC` channel, #mixxx on `Freenode "
"<http://www.freenode.net/>`_"
msgstr "Join our :term:`IRC` channel, #mixxx on `Freenode <http://www.freenode.net/>`_"

#: ../../source/chapters/getting_involved.rst:44
msgid ""
"Tell us your ideas! Email us, talk on IRC, file wishlist :term:`bugs <bug "
"report>`, or post on the forums."
msgstr "Tell us your ideas! Email us, talk on IRC, file wishlist :term:`bugs <bug report>`, or post on the forums."

#: ../../source/chapters/getting_involved.rst:46
msgid ""
"Register an account on our development platform `Github "
"<https://github.com/mixxxdj>`_ and fork our code. See `Using Git for Mixxx "
"Development <http://mixxx.org/wiki/doku.php/using_git>`_"
msgstr "Register an account on our development platform `Github <https://github.com/mixxxdj>`_ and fork our code. See `Using Git for Mixxx Development <http://mixxx.org/wiki/doku.php/using_git>`_"

#: ../../source/chapters/getting_involved.rst:49
msgid ""
"Join the `Mixxx Development Team "
"<https://launchpad.net/~mixxxcontributors/+join>`_ on Launchpad."
msgstr "Join the `Mixxx Development Team <https://launchpad.net/~mixxxcontributors/+join>`_ on Launchpad."

#: ../../source/chapters/getting_involved.rst:52
msgid ""
"Get familiar with the code. Pick a bug off of the `Easy Bug List "
"<https://bugs.launchpad.net/mixxx/+bugs?field.tag=easy>`_"
msgstr "Get familiar with the code. Pick a bug off of the `Easy Bug List <https://bugs.launchpad.net/mixxx/+bugs?field.tag=easy>`_"

#: ../../source/chapters/getting_involved.rst:54
msgid ""
"Get help fixing the bug on IRC, talk to us about the best way to do it."
msgstr "Get help fixing the bug on IRC, talk to us about the best way to do it."

#: ../../source/chapters/getting_involved.rst:56
msgid "**Contributing to Mixxx in 3 Easy Steps:**"
msgstr "**Contributing to Mixxx in 3 Easy Steps:**"

#: ../../source/chapters/getting_involved.rst:58
msgid ""
"Pick a bug off of the `Easy Bug List "
"<https://bugs.launchpad.net/mixxx/+bugs?field.tag=easy>`_"
msgstr "Pick a bug off of the `Easy Bug List <https://bugs.launchpad.net/mixxx/+bugs?field.tag=easy>`_"

#: ../../source/chapters/getting_involved.rst:60
msgid "Talk to us on IRC for help on how to fix it."
msgstr "Talk to us on IRC for help on how to fix it."

#: ../../source/chapters/getting_involved.rst:61
msgid ""
"Fix it! *Done!* Your name will now be on the `Mixxx contributor list "
"<http://mixxx.org/contact/>`_."
msgstr "Fix it! *Done!* Your name will now be on the `Mixxx contributor list <http://mixxx.org/contact/>`_."
